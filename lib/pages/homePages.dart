// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:quiz_app/constants/textstyle.dart';
import 'package:quiz_app/useFiles.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget Buttons(dynamic btnstyle, String btntext, void func()) {
    return SizedBox(
      width: 300,
      height: 55,
      child: ElevatedButton(
        style: btnstyle,
        onPressed: () {
          // setState(() {
          func();
          // });
        },
        child: Text(
          '$btntext',
          style: const TextStyle(fontSize: 30),
        ),
      ),
    );
  }

  UseQuiz useQuiz = UseQuiz();
  List<Icon> ikonka = [];
  void teksher(bool joop) {
    setState(() {
      if (useQuiz.isFinish()) {
        showDialog<void>(
          context: context,
          barrierDismissible: false, // user must tap button!
          builder: (BuildContext context) {
            return AlertDialog(
              // <-- SEE HERE
              title: const Text('Cancel booking'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: const <Widget>[
                    Text('Are you sure want to cancel booking?'),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('No'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('Yes'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
        useQuiz.indZero();
        ikonka = [];
      } else {
        if (useQuiz.joopAluu() == joop) {
          ikonka.add(const Icon(
            Icons.check,
            color: Colors.green,
          ));
        } else {
          ikonka.add(const Icon(
            Icons.close,
            color: Colors.red,
          ));
        }
        useQuiz.questions();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 32, 31, 31),
      appBar: AppBar(
        title: const Text(
          'Тапшырма 7',
          style: Styles.appBarText,
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              useQuiz.surooAluu(),
              style: Styles.bodyText,
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 48,
            ),
            Buttons(Styles.truebtn, 'Tuura', () => teksher(true)),
            const SizedBox(
              height: 18,
            ),
            Buttons(Styles.falsebtn, 'Tuura emes', () => teksher(false)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: ikonka,
            )
          ],
        ),
      ),
    );
  }
}
