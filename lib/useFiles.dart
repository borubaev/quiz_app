// ignore_for_file: file_names

import 'package:quiz_app/model.dart';

class UseQuiz {
  int index = 0;
  List<QuizModel> test = [
    QuizModel(suroo: 'Кыргызстанда 7 область барбы?', joop: true),
    QuizModel(suroo: 'Сулайман-Тоо Баткен шааарындабы?', joop: false),
    QuizModel(suroo: 'Кыргызстан коозбу?', joop: true),
    QuizModel(suroo: 'Кыргызстан баарына жагабы?', joop: true),
  ];
  String surooAluu() {
    return test[index].suroo;
  }

  bool joopAluu() {
    return test[index].joop;
  }

  void questions() {
    if (index <= test.length) {
      index++;
    }
  }

  void indZero() {
    index = 0;
  }

  bool isFinish() {
    if (test[index] == test.last) {
      return true;
    } else {
      return false;
    }
  }
}
