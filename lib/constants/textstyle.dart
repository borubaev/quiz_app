import 'package:flutter/material.dart';

class Styles {
  static const appBarText = TextStyle(color: Colors.black, fontSize: 25);
  static const bodyText = TextStyle(color: Colors.white, fontSize: 38);
  static const truebtn =
      ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.green));
  static const falsebtn =
      ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.red));
}
